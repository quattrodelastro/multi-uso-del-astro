﻿<?php 
include("header.php"); 
include("baza.php");

function genVpisna() {
	global $con;
	
	$query = "SELECT max(vpisna_st) from student";
	if (($result = $con->query($query)) !== FALSE) {
		$vpisnaM = $result->fetch_assoc()["max(vpisna_st)"];
		$leto = substr($vpisnaM, 2, 2);
		if ($leto != date("y")) {
			return "0000";
		}		
		$vpis4 = sprintf("%'04d", intval(substr($vpisnaM, 4, 4)) + 1);
		return $vpis4;
	}
}

if (isset($_FILES["filename"])) {
	if (is_uploaded_file($_FILES["filename"]["tmp_name"])) {
		if ($_FILES["filename"]["error"] > 0) {
			echo "Error: " . $_FILES["filename"]["error"] . "<br />";
		}
		elseif ($_FILES["filename"]["type"] != "text/plain") {
			echo "File must be a .txt";
		}
		else {
			// nalaganje datoteke
			$target_dir = "uploads/";
			$target_file = $target_dir . basename($_FILES["filename"]["name"]);
			$uploadOk = 1;
			if ($_FILES['filename']['type'] != 'text/plain') {
				$uploadOk = 0;
			}
			
			if (move_uploaded_file($_FILES["filename"]["tmp_name"], $target_file)) {
			} else {
				echo "Sorry, there was an error uploading your file.";
			}

			// parsanje datoteke
			$fp = fopen($target_file, 'r');
			while (!feof($fp)) {
				$line = fgets($fp);
				$name = "";
				
				$name = substr($line, 0, 30);
				$name = trim($name);
				$surname = "";
				
				$surname = substr($line, 30, 30);
				$surname = trim($surname);
				$sifra = "";
				
				$sifra = substr($line, 60, 7);
				$mail = "";
				
				$mail = substr($line, 67, 60);
				$mail = trim($mail);
				
				// generiraj vpisno
				$vpisna = "63" .  date("y") . genVpisna();
				// generiraj geslo
				$pass = $surname . "zamenenjaj";
				
				//vnesi novega uporabnika
				$query = "INSERT INTO student (vpisna_st, ime_studenta, priimek_studenta, student_email, student_pass) VALUES (\"" 
						. $vpisna
						. "\",\"" .  $name
						. "\",\"" . $surname
						. "\",\"" . $mail
						. "\",\"" . $pass
						."\")";
				if ($result = $con->query($query) === FALSE) {
					die('\nQuery error1' . $con->error);
				}
			}
			echo "<h2>Uvoz je bil uspešen.</h2>";
		}
	}
}
?>

<div class="col-md-6 col-md-offset-3">
	<div class="central-form">
		<form name="loginform" class="form-inline" method="POST" enctype="multipart/form-data" action="importlist.php">
			<div class="form-group">
		    	<label for="inputList">Uvoz seznama sprejetih študentov</label>
		    	<input type="file" id="inputList" name="filename">
		    	<p class="help-block">Seznam študentov mora biti v formatu TXT.</p>
		  	</div>
		  	<p class="text-right"><button type="submit" name="subbutt" class="btn btn-default">Uvozi</button></p>
		</form>
	</div>
</div>
<?php include("footer.php"); ?>