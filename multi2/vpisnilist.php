﻿<?php
include("baza.php");
global $con;

if (isset($_POST["a"])) {
	
	$vpisna_st = $_POST["vpisna_st"];
	// vpisna mora obstajati (? - glej TODO)
	$query = "SELECT COUNT(vpisna_st) as C FROM student WHERE vpisna = " . $vpisna_st;
	if (($result = $con->query($query)) !== FALSE) {
		$c = $result->fetch_assoc();
		$c = $c["C"];
		if ($c != 1) {
			die ("Vpisna številka ne obstaja.");
		}
	} else {
		// dbg
		echo $con->error;
		die (" Napaka v poizvedbi");
	}
	
	$ime = $_POST["ime"];
	$priimek = $_POST["priimek"];
	$datum_roj = $_POST["datum_roj"];
	// preveri smiselnost datuma TODO kakšn je tip datum v SQL
	// preverjanje v js, torej ne tuki???
	
	$kraj_roj = $_POST["kraj_roj"];
	// preveri obstoj kraja TODO v bazi mora bit šifrant krajev
	
	$spol = $_POST["spol"];
	$emso = $_POST["emso"];
	$email = $_POST["email"];
	// preveri ce je isti za pravo vpisno že v bazi, sicer IDK
	// preverjen v js
	$ulica_st = $_POST["ulica_st"];
	$ulica_zacasno = $_POST["ulica_zacasno"];
	$drzava_roj = $_POST["drzava_roj"];
	// TODO sifrant ali je kraj roj v drz roj
	$obcina_roj = $_POST["obcina_roj"];
	// TODO sifrant ali je kraj roj v obc roj
	$davcna_st = $_POST["davcna_st"];
	$telefon = $_POST["telefon"];
	$drzavljanstvo = $_POST["drzavljanstvo"];
	
	$drz_zacasno = $_POST["drz_zacasno"];
	$kraj_zacasno = $_POST["kraj_zacasno"];
	// TODO sifrant kraj v drzavi
	$drz_st = $_POST["drz_st"];
	$kraj_st = $_POST["kraj_st"];
	// TODO sifrant kraj v drzavi
	
	
	$query = "INSERT INTO student 
			() VALUES 
			()";
	
}
?>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <h3>Vpisni list</h3>
        
        <!-- Nekateri podatki morajo biti že prikazani? -->
        
        <form class="form-horizontal">
            <!--<h4 class="col-md-4 col-md-offset-2">Osebni podatki</h4> -->
            <!-- IME -->
            <div class="form-group">
              <label for="ime" class="col-md-4 control-label">Ime</label>
              <div class="col-md-5">
                <input type="text" class="form-control" id="ime">
              </div>
            </div>
            <!-- PRIIMEK -->
            <div class="form-group">
              <label for="priimek" class="col-md-4 control-label">Priimek</label>
              <div class="col-md-5">
                <input type="text" class="form-control" id="priimek">
              </div>
            </div>
            <!-- DATUM ROJSTVA -->
            <div class="form-group">
              <label for="rdatum" class="col-md-4 control-label">Datum rojstva</label>
              <div class="col-md-5">
                <input id="rdatum" type="date" maxlength="10" placeholder="DDMMYYYY" />
              </div>
            </div>
            <!-- KRAJ ROJSTVA -->
            <!-- vrjetno je bolš da damo textarea, pa potem preverjamo, kot pa dropdown? -->
            <div class="form-group">
              <label for="rkraj" class="col-md-4 control-label">Kraj rojstva</label>
              <div class="col-md-5">
                <input type="text" class="form-control" id="rkraj">
              </div>
            </div>
            <div class="form-group">
              <label for="spol" class="col-md-4 control-label">Spol</label>
              <div id="spol" class="col-md-5">
                <label class="radio-inline"><input type="radio" name="inlineRadioOptions" id="zenski" value="option1">ženski</label>
                <label class="radio-inline"><input type="radio" name="inlineRadioOptions" id="moski" value="option2">moški</label>
                <label class="radio-inline"><input type="radio" name="inlineRadioOptions" id="drugo" value="option3">drugo</label>
              </div>
            </div>
            <div class="form-group">
              <label for="emso" class="col-md-4 control-label">EMŠO</label>
              <div id="emso" class="col-md-5">
                <input type="text" class="form-control"></input>
              </div>
            </div>
            <div class="form-group">
              <label for="email" class="col-md-4 control-label">E-mail</label>
              <div id="email" class="col-md-5">
                <input type="text" class="form-control"></input>
              </div>
            </div>

            <h4 class="col-md-offset-2">Stalni naslov</h4>
            <div class="form-group">
              <label for="ulicast" class="col-md-4 control-label">Ulica in hišna številka</label>
              <div id="ulicast" class="col-md-5">
                <input type="text" class="form-control"></input>
              </div>
            </div>
            <div class="form-group">
              <label for="kraj" class="col-md-4 control-label">Kraj</label>
              <div id="email" class="col-md-5">
                <input type="text" class="form-control"></input>
              </div>
            </div>
            <hr />
        </form>
    </div>
</div>
<?php include("footer.php"); ?>

