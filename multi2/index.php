<?php include("header.php"); ?>

<div class="col-md-6 col-md-offset-3">
	<div class="central-form">
	<form name="loginform" class="form-horizontal" id="loginform">
		<div class="form-group">
	    	<label for="inputEmail" class="col-sm-2 control-label">E-mail:</label>
	    	<div class="col-sm-10">
	      		<input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="e-mail">
	    	</div>
		</div>
		
		<p id="regexEmailText" class="help-block col-sm-10 col-md-offset-2"></p>
		
		<div class="form-group">
		    <label for="inputPassword" class="col-sm-2 control-label">Geslo:</label>
		    <div class="col-sm-10">
		    	<input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="geslo">
		    </div>
		</div>
	  	<p class="text-right"><button type="submit" id="login-butt" class="btn btn-default">Login</button></p>
		
		<p id="LoginErrText" class="help-block"></p>
		
		<p class="help-block">Vaše uporabniško ime je elektronski naslov, ki ste ga podali pri vpisu, geslo pa vpisna številka. Prosimo, da geslo takoj po prijavi spremenite. V primeru težav kontaktirajte <a href="#">skrbnika sistema</a>.</p>
		<p class="help-block"><a href="retrievepwd.php">Pozabljeno geslo?</a></p>
	</form>
	</div>
</div>
<?php include("footer.php"); ?>