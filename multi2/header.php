<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MultiUsoDelAstro</title>
	
    <script src="js/jquery-1.11.2.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/customstyle.css" rel="stylesheet">
	  <script src="js/bootstrap.js"></script>
    <script src="js/customjs.js"></script>

  </head>
  <body>
    
  <div class="container-fluid">
      <div class="row" id="toprow">
          <div class="col-md-8 col-md-offset-2">
              <div class="row">
                <div class="col-md-6">
                  <h2>MultiUsoDelAstro</h2>
                </div>
                <div class="col-md-6">
                  <p id="headp">Univerza v Ljubljani<br />Fakulteta za računalništvo in informatiko</p>
                </div>
              </div>
          </div>
      </div>
    <div class="row" id="contentrow">