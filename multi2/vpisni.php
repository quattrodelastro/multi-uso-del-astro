<?php include("header.php"); ?>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <h3>Vpisni list</h3>
        
        <!-- Nekateri podatki morajo biti že prikazani? -->
        
        <form class="form-horizontal">
            <!--<h4 class="col-md-4 col-md-offset-2">Osebni podatki</h4> -->
            <!-- IME -->
            <div class="form-group">
              <label for="ime" class="col-md-4 control-label">Ime</label>
              <div class="col-md-5">
                <input type="text" class="form-control" id="ime">
              </div>
            </div>
            <!-- PRIIMEK -->
            <div class="form-group">
              <label for="priimek" class="col-md-4 control-label">Priimek</label>
              <div class="col-md-5">
                <input type="text" class="form-control" id="priimek">
              </div>
            </div>
            <!-- DATUM ROJSTVA -->
            <div class="form-group">
              <label for="rdatum" class="col-md-4 control-label">Datum rojstva</label>
              <div class="col-md-5">
                <input id="rdatum" type="date" maxlength="10" placeholder="DDMMYYYY" />
              </div>
            </div>
            <!-- KRAJ ROJSTVA -->
            <!-- vrjetno je bolš da damo textarea, pa potem preverjamo, kot pa dropdown? -->
            <div class="form-group">
              <label for="rkraj" class="col-md-4 control-label">Kraj rojstva</label>
              <div class="col-md-5">
                <input type="text" class="form-control" id="rkraj">
              </div>
            </div>
            <div class="form-group">
              <label for="spol" class="col-md-4 control-label">Spol</label>
              <div id="spol" class="col-md-5">
                <label class="radio-inline"><input type="radio" name="inlineRadioOptions" id="zenski" value="option1">ženski</label>
                <label class="radio-inline"><input type="radio" name="inlineRadioOptions" id="moski" value="option2">moški</label>
                <label class="radio-inline"><input type="radio" name="inlineRadioOptions" id="drugo" value="option3">drugo</label>
              </div>
            </div>
            <div class="form-group">
              <label for="emso" class="col-md-4 control-label">EMŠO</label>
              <div id="emso" class="col-md-5">
                <input type="text" class="form-control"></input>
              </div>
            </div>
            <div class="form-group">
              <label for="email" class="col-md-4 control-label">E-mail</label>
              <div id="email" class="col-md-5">
                <input type="text" class="form-control"></input>
              </div>
            </div>

            <h4 class="col-md-offset-2">Stalni naslov</h4>
            <div class="form-group">
              <label for="ulicast" class="col-md-4 control-label">Ulica in hišna številka</label>
              <div id="ulicast" class="col-md-5">
                <input type="text" class="form-control"></input>
              </div>
            </div>
            <div class="form-group">
              <label for="kraj" class="col-md-4 control-label">Kraj</label>
              <div id="email" class="col-md-5">
                <input type="text" class="form-control"></input>
              </div>
            </div>
            <hr />
        </form>
    </div>
</div>
<?php include("footer.php"); ?>