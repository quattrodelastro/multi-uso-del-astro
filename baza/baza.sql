-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema t16_2015
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema t16_2015
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `t16_2015` DEFAULT CHARACTER SET utf8 ;
USE `t16_2015` ;

-- -----------------------------------------------------
-- Table `t16_2015`.`drzava`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `t16_2015`.`drzava` (
  `id_drzava` INT(11) NOT NULL,
  `ime_drzava` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id_drzava`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `t16_2015`.`modul`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `t16_2015`.`modul` (
  `id_modul` INT(1) NOT NULL,
  `ime_modula` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id_modul`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `t16_2015`.`predmet`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `t16_2015`.`predmet` (
  `id_predmet` INT(11) NOT NULL,
  `ime_predmet` VARCHAR(45) NULL DEFAULT NULL,
  `f_modul` INT(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id_predmet`),
  INDEX `modul_idx` (`f_modul` ASC),
  CONSTRAINT `modul`
    FOREIGN KEY (`f_modul`)
    REFERENCES `t16_2015`.`modul` (`id_modul`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `t16_2015`.`profesor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `t16_2015`.`profesor` (
  `id_profesor` INT(11) NOT NULL,
  `ime_profesor` VARCHAR(45) NULL DEFAULT NULL,
  `priimek_profesor` VARCHAR(45) NULL DEFAULT NULL,
  `profesor_izvedba_predmeta` INT(11) NULL DEFAULT NULL,
  `profesor_email` VARCHAR(45) NULL DEFAULT NULL,
  `profesor_pass` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id_profesor`),
  INDEX `izvedba_predmeta_idx` (`profesor_izvedba_predmeta` ASC),
  CONSTRAINT `izvedba_predmeta`
    FOREIGN KEY (`profesor_izvedba_predmeta`)
    REFERENCES `t16_2015`.`izvedba_predmeta` (`id_izvedba_predmeta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `t16_2015`.`izvedba_predmeta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `t16_2015`.`izvedba_predmeta` (
  `id_izvedba_predmeta` INT(11) NOT NULL,
  `f_profesor` INT(11) NULL DEFAULT NULL,
  `f_predmet` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_izvedba_predmeta`),
  INDEX `prof_idx` (`f_profesor` ASC),
  INDEX `pred_idx` (`f_predmet` ASC),
  CONSTRAINT `pred`
    FOREIGN KEY (`f_predmet`)
    REFERENCES `t16_2015`.`predmet` (`id_predmet`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `prof`
    FOREIGN KEY (`f_profesor`)
    REFERENCES `t16_2015`.`profesor` (`id_profesor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `t16_2015`.`letnik`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `t16_2015`.`letnik` (
  `id_letnik` VARCHAR(3) NOT NULL,
  PRIMARY KEY (`id_letnik`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `t16_2015`.`nacin_studija`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `t16_2015`.`nacin_studija` (
  `id_nacin_studija` INT(11) NOT NULL,
  PRIMARY KEY (`id_nacin_studija`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `t16_2015`.`obcina`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `t16_2015`.`obcina` (
  `id_obcina` INT(11) NOT NULL,
  `ime_obcina` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id_obcina`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `t16_2015`.`oblika_studija`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `t16_2015`.`oblika_studija` (
  `id_oblika_studija` INT(11) NOT NULL,
  PRIMARY KEY (`id_oblika_studija`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `t16_2015`.`posta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `t16_2015`.`posta` (
  `id_posta` INT(4) NOT NULL,
  `ime_posta` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id_posta`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `t16_2015`.`sestavni_del_predmetnika`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `t16_2015`.`sestavni_del_predmetnika` (
  `id_sestavni_del_predmetnika` INT(11) NOT NULL,
  PRIMARY KEY (`id_sestavni_del_predmetnika`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `t16_2015`.`studijski_program`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `t16_2015`.`studijski_program` (
  `id_studijski_program` INT(11) NOT NULL,
  PRIMARY KEY (`id_studijski_program`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `t16_2015`.`predmet_stud_programa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `t16_2015`.`predmet_stud_programa` (
  `id_predmet_stud_programa` INT(11) NOT NULL,
  `f_predmet` INT(11) NULL DEFAULT NULL,
  `f_sestavni_del_pr` INT(11) NULL DEFAULT NULL,
  `f_stud_prog` INT(11) NULL DEFAULT NULL,
  `f_letnik` VARCHAR(3) NULL DEFAULT NULL,
  `f_izvedba_predmeta` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_predmet_stud_programa`),
  INDEX `predmet_idx` (`f_predmet` ASC),
  INDEX `sest_del_pred_idx` (`f_sestavni_del_pr` ASC),
  INDEX `stud_prog_idx` (`f_stud_prog` ASC),
  INDEX `letnik1_idx` (`f_letnik` ASC),
  INDEX `izvedba_pred_idx` (`f_izvedba_predmeta` ASC),
  CONSTRAINT `izvedba_pred`
    FOREIGN KEY (`f_izvedba_predmeta`)
    REFERENCES `t16_2015`.`izvedba_predmeta` (`id_izvedba_predmeta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `sest_del_pred`
    FOREIGN KEY (`f_sestavni_del_pr`)
    REFERENCES `t16_2015`.`sestavni_del_predmetnika` (`id_sestavni_del_predmetnika`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `stud_prog`
    FOREIGN KEY (`f_stud_prog`)
    REFERENCES `t16_2015`.`studijski_program` (`id_studijski_program`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `t16_2015`.`prijava`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `t16_2015`.`prijava` (
  `id_prijava` INT(11) NOT NULL,
  PRIMARY KEY (`id_prijava`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `t16_2015`.`referent`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `t16_2015`.`referent` (
  `id_referent` INT(11) NOT NULL,
  `referent_email` VARCHAR(45) NULL DEFAULT NULL,
  `referent_pass` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id_referent`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `t16_2015`.`skrbnik`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `t16_2015`.`skrbnik` (
  `id_skrbnik` INT(11) NOT NULL,
  `skrbnik_email` VARCHAR(45) NULL DEFAULT NULL,
  `skrbnik_pass` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id_skrbnik`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `t16_2015`.`vpisani_predmet`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `t16_2015`.`vpisani_predmet` (
  `id_vpisani_predmet` INT(11) NOT NULL,
  PRIMARY KEY (`id_vpisani_predmet`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `t16_2015`.`vrsta_vpisa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `t16_2015`.`vrsta_vpisa` (
  `id_vrsta_vpisa` INT(11) NOT NULL,
  PRIMARY KEY (`id_vrsta_vpisa`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `t16_2015`.`vzporedni_studij`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `t16_2015`.`vzporedni_studij` (
  `id_vzporedni_studij` INT(11) NOT NULL,
  `zavod` VARCHAR(45) NULL DEFAULT NULL,
  `studijski_program` VARCHAR(45) NULL DEFAULT NULL,
  `letnik` VARCHAR(45) NULL DEFAULT NULL,
  `vrsta_studija` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id_vzporedni_studij`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `t16_2015`.`zeton`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `t16_2015`.`zeton` (
  `id_zeton` INT(11) NOT NULL,
  PRIMARY KEY (`id_zeton`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `t16_2015`.`vpis`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `t16_2015`.`vpis` (
  `id_vpis` INT(11) NOT NULL,
  `vpis_vrsta` INT(11) NULL DEFAULT NULL,
  `vpis_nacin_st` INT(11) NULL DEFAULT NULL,
  `vpis_oblika_st` INT(11) NULL DEFAULT NULL,
  `vpis_vpisani_predmet` INT(11) NULL DEFAULT NULL,
  `vpis_studijski_program` INT(11) NULL DEFAULT NULL,
  `vpis_letnik` VARCHAR(3) NULL DEFAULT NULL,
  `vpis_zeton` INT(11) NULL DEFAULT NULL,
  `vpis_posta` INT(4) NULL DEFAULT NULL,
  `izbirna_skupina` VARCHAR(11) NULL DEFAULT NULL,
  `leto_vpisa` VARCHAR(9) NULL DEFAULT NULL,
  `vrsta_studija` VARCHAR(45) NULL DEFAULT NULL,
  `smer_studija` VARCHAR(45) NULL DEFAULT NULL,
  `vzporedni_studij` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_vpis`),
  INDEX `zeton_idx` (`vpis_zeton` ASC),
  INDEX `vrsta_vpisa_idx` (`vpis_vrsta` ASC),
  INDEX `nacin_studija_idx` (`vpis_nacin_st` ASC),
  INDEX `oblika_studija_idx` (`vpis_oblika_st` ASC),
  INDEX `vpisani_predmet_idx` (`vpis_vpisani_predmet` ASC),
  INDEX `studijski_program_idx` (`vpis_studijski_program` ASC),
  INDEX `letnik_idx` (`vpis_letnik` ASC),
  INDEX `posta1_idx` (`vpis_posta` ASC),
  INDEX `vzporedni_idx` (`vzporedni_studij` ASC),
  CONSTRAINT `nacin_studija`
    FOREIGN KEY (`vpis_nacin_st`)
    REFERENCES `t16_2015`.`nacin_studija` (`id_nacin_studija`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `oblika_studija`
    FOREIGN KEY (`vpis_oblika_st`)
    REFERENCES `t16_2015`.`oblika_studija` (`id_oblika_studija`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `posta1`
    FOREIGN KEY (`vpis_posta`)
    REFERENCES `t16_2015`.`posta` (`id_posta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `studijski_program`
    FOREIGN KEY (`vpis_studijski_program`)
    REFERENCES `t16_2015`.`studijski_program` (`id_studijski_program`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `vpisani_predmet`
    FOREIGN KEY (`vpis_vpisani_predmet`)
    REFERENCES `t16_2015`.`vpisani_predmet` (`id_vpisani_predmet`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `vrsta`
    FOREIGN KEY (`vpis_vrsta`)
    REFERENCES `t16_2015`.`vrsta_vpisa` (`id_vrsta_vpisa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `vzporedni`
    FOREIGN KEY (`vzporedni_studij`)
    REFERENCES `t16_2015`.`vzporedni_studij` (`id_vzporedni_studij`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `zeton1`
    FOREIGN KEY (`vpis_zeton`)
    REFERENCES `t16_2015`.`zeton` (`id_zeton`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `t16_2015`.`student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `t16_2015`.`student` (
  `vpisna_st` INT(11) NOT NULL,
  `ime_studenta` VARCHAR(45) NULL DEFAULT NULL,
  `priimek_studenta` VARCHAR(45) NULL DEFAULT NULL,
  `student_posta` INT(4) NULL DEFAULT NULL,
  `student_drzava` INT(11) NULL DEFAULT NULL,
  `student_obcina` INT(11) NULL DEFAULT NULL,
  `student_prijava` INT(11) NULL DEFAULT NULL,
  `student_zeton` INT(11) NULL DEFAULT NULL,
  `student_vpis` INT(11) NULL DEFAULT NULL,
  `datum_rojstva_st` DATETIME NULL DEFAULT NULL,
  `student_email` VARCHAR(45) NULL DEFAULT NULL,
  `student_pass` VARCHAR(45) NULL DEFAULT NULL,
  `davcna_st` INT(13) NULL DEFAULT NULL,
  `spol_st` VARCHAR(1) NULL DEFAULT NULL,
  `mobitel_st` VARCHAR(12) NULL DEFAULT NULL,
  `naslov_stalni` VARCHAR(45) NULL DEFAULT NULL,
  `naslov_zacasni` VARCHAR(45) NULL DEFAULT NULL,
  `student_drzavljanstvo` INT(11) NULL DEFAULT NULL,
  `emso` INT(13) NULL DEFAULT NULL,
  PRIMARY KEY (`vpisna_st`),
  INDEX `posta_idx` (`student_posta` ASC, `student_drzava` ASC),
  INDEX `drzava_idx` (`student_drzava` ASC),
  INDEX `obcina_idx` (`student_obcina` ASC),
  INDEX `prijava_idx` (`student_prijava` ASC),
  INDEX `zeton_idx` (`student_zeton` ASC),
  INDEX `vpis_idx` (`student_vpis` ASC),
  INDEX `drzavljanstvo_idx` (`student_drzavljanstvo` ASC),
  CONSTRAINT `drzava`
    FOREIGN KEY (`student_drzava`)
    REFERENCES `t16_2015`.`drzava` (`id_drzava`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `drzavljanstvo`
    FOREIGN KEY (`student_drzavljanstvo`)
    REFERENCES `t16_2015`.`drzava` (`id_drzava`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `obcina`
    FOREIGN KEY (`student_obcina`)
    REFERENCES `t16_2015`.`obcina` (`id_obcina`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `posta`
    FOREIGN KEY (`student_posta`)
    REFERENCES `t16_2015`.`posta` (`id_posta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `prijava`
    FOREIGN KEY (`student_prijava`)
    REFERENCES `t16_2015`.`prijava` (`id_prijava`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `vpis`
    FOREIGN KEY (`student_vpis`)
    REFERENCES `t16_2015`.`vpis` (`id_vpis`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `zeton`
    FOREIGN KEY (`student_zeton`)
    REFERENCES `t16_2015`.`zeton` (`id_zeton`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `t16_2015`.`studijsko_leto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `t16_2015`.`studijsko_leto` (
  `id_studijsko_leto` INT(11) NOT NULL,
  PRIMARY KEY (`id_studijsko_leto`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
