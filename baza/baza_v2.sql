﻿-- phpMyAdmin SQL Dump
-- version 4.2.6deb1
-- http://www.phpmyadmin.net
--
-- Gostitelj: localhost
-- Čas nastanka: 02. apr 2015 ob 10.37
-- Različica strežnika: 5.5.41-0ubuntu0.14.10.1
-- Različica PHP: 5.5.12-2ubuntu4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Zbirka podatkov: `t16_2015`
--

-- --------------------------------------------------------

--
-- Struktura tabele `drzava`
--

DROP TABLE IF EXISTS `drzava`;
CREATE TABLE IF NOT EXISTS `drzava` (
  `id_drzava` int(11) NOT NULL,
  `ime_drzava` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabele `izvedba_predmeta`
--

DROP TABLE IF EXISTS `izvedba_predmeta`;
CREATE TABLE IF NOT EXISTS `izvedba_predmeta` (
  `id_izvedba_predmeta` int(11) NOT NULL,
  `f_profesor` int(11) DEFAULT NULL,
  `f_predmet` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabele `letnik`
--

DROP TABLE IF EXISTS `letnik`;
CREATE TABLE IF NOT EXISTS `letnik` (
  `id_letnik` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabele `modul`
--

DROP TABLE IF EXISTS `modul`;
CREATE TABLE IF NOT EXISTS `modul` (
  `id_modul` int(1) NOT NULL,
  `ime_modula` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabele `nacin_studija`
--

DROP TABLE IF EXISTS `nacin_studija`;
CREATE TABLE IF NOT EXISTS `nacin_studija` (
  `id_nacin_studija` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabele `obcina`
--

DROP TABLE IF EXISTS `obcina`;
CREATE TABLE IF NOT EXISTS `obcina` (
  `id_obcina` int(11) NOT NULL,
  `ime_obcina` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabele `oblika_studija`
--

DROP TABLE IF EXISTS `oblika_studija`;
CREATE TABLE IF NOT EXISTS `oblika_studija` (
  `id_oblika_studija` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabele `posta`
--

DROP TABLE IF EXISTS `posta`;
CREATE TABLE IF NOT EXISTS `posta` (
  `id_posta` int(4) NOT NULL,
  `ime_posta` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabele `predmet`
--

DROP TABLE IF EXISTS `predmet`;
CREATE TABLE IF NOT EXISTS `predmet` (
  `id_predmet` int(11) NOT NULL,
  `ime_predmet` varchar(45) DEFAULT NULL,
  `f_modul` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabele `predmet_stud_programa`
--

DROP TABLE IF EXISTS `predmet_stud_programa`;
CREATE TABLE IF NOT EXISTS `predmet_stud_programa` (
  `id_predmet_stud_programa` int(11) NOT NULL,
  `f_predmet` int(11) DEFAULT NULL,
  `f_sestavni_del_pr` int(11) DEFAULT NULL,
  `f_stud_prog` int(11) DEFAULT NULL,
  `f_letnik` varchar(3) DEFAULT NULL,
  `f_izvedba_predmeta` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabele `prijava`
--

DROP TABLE IF EXISTS `prijava`;
CREATE TABLE IF NOT EXISTS `prijava` (
  `id_prijava` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabele `profesor`
--

DROP TABLE IF EXISTS `profesor`;
CREATE TABLE IF NOT EXISTS `profesor` (
  `id_profesor` int(11) NOT NULL,
  `ime_profesor` varchar(45) DEFAULT NULL,
  `priimek_profesor` varchar(45) DEFAULT NULL,
  `profesor_izvedba_predmeta` int(11) DEFAULT NULL,
  `profesor_email` varchar(45) DEFAULT NULL,
  `profesor_pass` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Odloži podatke za tabelo `profesor`
--

INSERT INTO `profesor` (`id_profesor`, `ime_profesor`, `priimek_profesor`, `profesor_izvedba_predmeta`, `profesor_email`, `profesor_pass`) VALUES
(406, NULL, NULL, NULL, 'mojca_p@gmail.com', 'mojcamojca'),
(444, NULL, NULL, NULL, 'anita_dobnik@gmail.com', 'nita_kopita'),
(456, NULL, NULL, NULL, 'filip_m@gmail.com', 'filemarecare');

-- --------------------------------------------------------

--
-- Struktura tabele `referent`
--

DROP TABLE IF EXISTS `referent`;
CREATE TABLE IF NOT EXISTS `referent` (
  `id_referent` int(11) NOT NULL,
  `referent_email` varchar(45) DEFAULT NULL,
  `referent_pass` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabele `sestavni_del_predmetnika`
--

DROP TABLE IF EXISTS `sestavni_del_predmetnika`;
CREATE TABLE IF NOT EXISTS `sestavni_del_predmetnika` (
  `id_sestavni_del_predmetnika` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabele `skrbnik`
--

DROP TABLE IF EXISTS `skrbnik`;
CREATE TABLE IF NOT EXISTS `skrbnik` (
  `id_skrbnik` int(11) NOT NULL,
  `skrbnik_email` varchar(45) DEFAULT NULL,
  `skrbnik_pass` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabele `student`
--

DROP TABLE IF EXISTS `student`;
CREATE TABLE IF NOT EXISTS `student` (
  `vpisna_st` int(11) NOT NULL,
  `ime` varchar(45) DEFAULT NULL,
  `priimek` varchar(45) DEFAULT NULL,
  `datum_rojstva` datetime DEFAULT NULL,
  `posta_rojstva` int(4) NOT NULL,
  `spol` varchar(1) DEFAULT NULL,
  `emso` int(13) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `naslov_stalni` varchar(45) DEFAULT NULL,
  `posta_stalni` int(4) DEFAULT NULL,
  `drzava_stalni` int(11) NOT NULL,
  `naslov_zacasni` varchar(45) DEFAULT NULL,
  `posta_zacasni` int(4) NOT NULL,
  `drzava_zacasni` int(11) NOT NULL,
  `drzava_rojstva` int(11) NOT NULL,
  `obcina_rojstva` int(11) DEFAULT NULL,
  `davcna_st` int(13) DEFAULT NULL,
  `mobitel_st` varchar(12) DEFAULT NULL,
  `student_drzavljanstvo` int(11) DEFAULT NULL,
  `student_prijava` int(11) DEFAULT NULL,
  `student_zeton` int(11) DEFAULT NULL,
  `student_vpis` int(11) DEFAULT NULL,
  `student_pass` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Odloži podatke za tabelo `student`
--

INSERT INTO `student` (`vpisna_st`, `ime`, `priimek`, `datum_rojstva`, `posta_rojstva`, `spol`, `emso`, `email`, `naslov_stalni`, `posta_stalni`, `drzava_stalni`, `naslov_zacasni`, `posta_zacasni`, `drzava_zacasni`, `drzava_rojstva`, `obcina_rojstva`, `davcna_st`, `mobitel_st`, `student_drzavljanstvo`, `student_prijava`, `student_zeton`, `student_vpis`, `student_pass`) VALUES
(63150016, '﻿Maj', 'Smerkol', NULL, 0, NULL, NULL, '211maj.smerkol@maj.com', NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Smerkolzamenenjaj'),
(63150017, 'Alja', 'Debeljak', NULL, 0, NULL, NULL, 'maj.smerkow@maj.com', NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Debeljakzamenenjaj'),
(63150018, 'Nika', 'Bric', NULL, 0, NULL, NULL, 'maj.smerwwl@maj.com', NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Briczamenenjaj'),
(63150019, 'Rok', 'Kušter', NULL, 0, NULL, NULL, '1maj.smwwkol@maj.com', NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kušterzamenenjaj'),
(63150020, '﻿Maj', 'Smerkol', NULL, 0, NULL, NULL, '211maj.smerkol@maj.com', NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Smerkolzamenenjaj'),
(63150021, 'Alja', 'Debeljak', NULL, 0, NULL, NULL, 'maj.smerkow@maj.com', NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Debeljakzamenenjaj'),
(63150022, 'Nika', 'Bric', NULL, 0, NULL, NULL, 'maj.smerwwl@maj.com', NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Briczamenenjaj'),
(63150023, 'Rok', 'Kušter', NULL, 0, NULL, NULL, '1maj.smwwkol@maj.com', NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kušterzamenenjaj');

-- --------------------------------------------------------

--
-- Struktura tabele `studijski_program`
--

DROP TABLE IF EXISTS `studijski_program`;
CREATE TABLE IF NOT EXISTS `studijski_program` (
  `id_studijski_program` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabele `studijsko_leto`
--

DROP TABLE IF EXISTS `studijsko_leto`;
CREATE TABLE IF NOT EXISTS `studijsko_leto` (
  `id_studijsko_leto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabele `vpis`
--

DROP TABLE IF EXISTS `vpis`;
CREATE TABLE IF NOT EXISTS `vpis` (
  `id_vpis` int(11) NOT NULL,
  `vpis_vrsta` int(11) DEFAULT NULL,
  `vpis_nacin_st` int(11) DEFAULT NULL,
  `vpis_oblika_st` int(11) DEFAULT NULL,
  `vpis_vpisani_predmet` int(11) DEFAULT NULL,
  `vpis_studijski_program` int(11) DEFAULT NULL,
  `vpis_letnik` varchar(3) DEFAULT NULL,
  `vpis_zeton` int(11) DEFAULT NULL,
  `vpis_posta` int(4) DEFAULT NULL,
  `izbirna_skupina` varchar(11) DEFAULT NULL,
  `leto_vpisa` varchar(9) DEFAULT NULL,
  `vrsta_studija` varchar(45) DEFAULT NULL,
  `smer_studija` varchar(45) DEFAULT NULL,
  `vzporedni_studij` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabele `vpisani_predmet`
--

DROP TABLE IF EXISTS `vpisani_predmet`;
CREATE TABLE IF NOT EXISTS `vpisani_predmet` (
  `id_vpisani_predmet` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabele `vrsta_vpisa`
--

DROP TABLE IF EXISTS `vrsta_vpisa`;
CREATE TABLE IF NOT EXISTS `vrsta_vpisa` (
  `id_vrsta_vpisa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabele `vzporedni_studij`
--

DROP TABLE IF EXISTS `vzporedni_studij`;
CREATE TABLE IF NOT EXISTS `vzporedni_studij` (
  `id_vzporedni_studij` int(11) NOT NULL,
  `zavod` varchar(45) DEFAULT NULL,
  `studijski_program` varchar(45) DEFAULT NULL,
  `letnik` varchar(45) DEFAULT NULL,
  `vrsta_studija` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabele `zeton`
--

DROP TABLE IF EXISTS `zeton`;
CREATE TABLE IF NOT EXISTS `zeton` (
  `id_zeton` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indeksi zavrženih tabel
--

--
-- Indeksi tabele `drzava`
--
ALTER TABLE `drzava`
 ADD PRIMARY KEY (`id_drzava`);

--
-- Indeksi tabele `izvedba_predmeta`
--
ALTER TABLE `izvedba_predmeta`
 ADD PRIMARY KEY (`id_izvedba_predmeta`), ADD KEY `prof_idx` (`f_profesor`), ADD KEY `pred_idx` (`f_predmet`);

--
-- Indeksi tabele `letnik`
--
ALTER TABLE `letnik`
 ADD PRIMARY KEY (`id_letnik`);

--
-- Indeksi tabele `modul`
--
ALTER TABLE `modul`
 ADD PRIMARY KEY (`id_modul`);

--
-- Indeksi tabele `nacin_studija`
--
ALTER TABLE `nacin_studija`
 ADD PRIMARY KEY (`id_nacin_studija`);

--
-- Indeksi tabele `obcina`
--
ALTER TABLE `obcina`
 ADD PRIMARY KEY (`id_obcina`);

--
-- Indeksi tabele `oblika_studija`
--
ALTER TABLE `oblika_studija`
 ADD PRIMARY KEY (`id_oblika_studija`);

--
-- Indeksi tabele `posta`
--
ALTER TABLE `posta`
 ADD PRIMARY KEY (`id_posta`);

--
-- Indeksi tabele `predmet`
--
ALTER TABLE `predmet`
 ADD PRIMARY KEY (`id_predmet`), ADD KEY `modul_idx` (`f_modul`);

--
-- Indeksi tabele `predmet_stud_programa`
--
ALTER TABLE `predmet_stud_programa`
 ADD PRIMARY KEY (`id_predmet_stud_programa`), ADD KEY `predmet_idx` (`f_predmet`), ADD KEY `sest_del_pred_idx` (`f_sestavni_del_pr`), ADD KEY `stud_prog_idx` (`f_stud_prog`), ADD KEY `letnik1_idx` (`f_letnik`), ADD KEY `izvedba_pred_idx` (`f_izvedba_predmeta`);

--
-- Indeksi tabele `prijava`
--
ALTER TABLE `prijava`
 ADD PRIMARY KEY (`id_prijava`);

--
-- Indeksi tabele `profesor`
--
ALTER TABLE `profesor`
 ADD PRIMARY KEY (`id_profesor`), ADD KEY `izvedba_predmeta_idx` (`profesor_izvedba_predmeta`);

--
-- Indeksi tabele `referent`
--
ALTER TABLE `referent`
 ADD PRIMARY KEY (`id_referent`);

--
-- Indeksi tabele `sestavni_del_predmetnika`
--
ALTER TABLE `sestavni_del_predmetnika`
 ADD PRIMARY KEY (`id_sestavni_del_predmetnika`);

--
-- Indeksi tabele `skrbnik`
--
ALTER TABLE `skrbnik`
 ADD PRIMARY KEY (`id_skrbnik`);

--
-- Indeksi tabele `student`
--
ALTER TABLE `student`
 ADD PRIMARY KEY (`vpisna_st`), ADD KEY `posta_idx` (`posta_stalni`), ADD KEY `obcina_idx` (`obcina_rojstva`), ADD KEY `prijava_idx` (`student_prijava`), ADD KEY `zeton_idx` (`student_zeton`), ADD KEY `vpis_idx` (`student_vpis`), ADD KEY `drzavljanstvo_idx` (`student_drzavljanstvo`);

--
-- Indeksi tabele `studijski_program`
--
ALTER TABLE `studijski_program`
 ADD PRIMARY KEY (`id_studijski_program`);

--
-- Indeksi tabele `studijsko_leto`
--
ALTER TABLE `studijsko_leto`
 ADD PRIMARY KEY (`id_studijsko_leto`);

--
-- Indeksi tabele `vpis`
--
ALTER TABLE `vpis`
 ADD PRIMARY KEY (`id_vpis`), ADD KEY `zeton_idx` (`vpis_zeton`), ADD KEY `vrsta_vpisa_idx` (`vpis_vrsta`), ADD KEY `nacin_studija_idx` (`vpis_nacin_st`), ADD KEY `oblika_studija_idx` (`vpis_oblika_st`), ADD KEY `vpisani_predmet_idx` (`vpis_vpisani_predmet`), ADD KEY `studijski_program_idx` (`vpis_studijski_program`), ADD KEY `letnik_idx` (`vpis_letnik`), ADD KEY `posta1_idx` (`vpis_posta`), ADD KEY `vzporedni_idx` (`vzporedni_studij`);

--
-- Indeksi tabele `vpisani_predmet`
--
ALTER TABLE `vpisani_predmet`
 ADD PRIMARY KEY (`id_vpisani_predmet`);

--
-- Indeksi tabele `vrsta_vpisa`
--
ALTER TABLE `vrsta_vpisa`
 ADD PRIMARY KEY (`id_vrsta_vpisa`);

--
-- Indeksi tabele `vzporedni_studij`
--
ALTER TABLE `vzporedni_studij`
 ADD PRIMARY KEY (`id_vzporedni_studij`);

--
-- Indeksi tabele `zeton`
--
ALTER TABLE `zeton`
 ADD PRIMARY KEY (`id_zeton`);

--
-- Omejitve tabel za povzetek stanja
--

--
-- Omejitve za tabelo `izvedba_predmeta`
--
ALTER TABLE `izvedba_predmeta`
ADD CONSTRAINT `pred` FOREIGN KEY (`f_predmet`) REFERENCES `predmet` (`id_predmet`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `prof` FOREIGN KEY (`f_profesor`) REFERENCES `profesor` (`id_profesor`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Omejitve za tabelo `predmet`
--
ALTER TABLE `predmet`
ADD CONSTRAINT `modul` FOREIGN KEY (`f_modul`) REFERENCES `modul` (`id_modul`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Omejitve za tabelo `predmet_stud_programa`
--
ALTER TABLE `predmet_stud_programa`
ADD CONSTRAINT `izvedba_pred` FOREIGN KEY (`f_izvedba_predmeta`) REFERENCES `izvedba_predmeta` (`id_izvedba_predmeta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `sest_del_pred` FOREIGN KEY (`f_sestavni_del_pr`) REFERENCES `sestavni_del_predmetnika` (`id_sestavni_del_predmetnika`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `stud_prog` FOREIGN KEY (`f_stud_prog`) REFERENCES `studijski_program` (`id_studijski_program`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Omejitve za tabelo `profesor`
--
ALTER TABLE `profesor`
ADD CONSTRAINT `izvedba_predmeta` FOREIGN KEY (`profesor_izvedba_predmeta`) REFERENCES `izvedba_predmeta` (`id_izvedba_predmeta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Omejitve za tabelo `student`
--
ALTER TABLE `student`
ADD CONSTRAINT `drzavljanstvo` FOREIGN KEY (`student_drzavljanstvo`) REFERENCES `drzava` (`id_drzava`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `prijava` FOREIGN KEY (`student_prijava`) REFERENCES `prijava` (`id_prijava`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `vpis` FOREIGN KEY (`student_vpis`) REFERENCES `vpis` (`id_vpis`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zeton` FOREIGN KEY (`student_zeton`) REFERENCES `zeton` (`id_zeton`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Omejitve za tabelo `vpis`
--
ALTER TABLE `vpis`
ADD CONSTRAINT `nacin_studija` FOREIGN KEY (`vpis_nacin_st`) REFERENCES `nacin_studija` (`id_nacin_studija`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `oblika_studija` FOREIGN KEY (`vpis_oblika_st`) REFERENCES `oblika_studija` (`id_oblika_studija`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `posta1` FOREIGN KEY (`vpis_posta`) REFERENCES `posta` (`id_posta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `studijski_program` FOREIGN KEY (`vpis_studijski_program`) REFERENCES `studijski_program` (`id_studijski_program`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `vpisani_predmet` FOREIGN KEY (`vpis_vpisani_predmet`) REFERENCES `vpisani_predmet` (`id_vpisani_predmet`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `vrsta` FOREIGN KEY (`vpis_vrsta`) REFERENCES `vrsta_vpisa` (`id_vrsta_vpisa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `vzporedni` FOREIGN KEY (`vzporedni_studij`) REFERENCES `vzporedni_studij` (`id_vzporedni_studij`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zeton1` FOREIGN KEY (`vpis_zeton`) REFERENCES `zeton` (`id_zeton`) ON DELETE NO ACTION ON UPDATE NO ACTION;